# infosys_app

Membuat project sederhana menggunakan Flutter dan state Management GetX

## List Account
- user; user123
- admin; admin123

## Library yang digunakan beserta alasan
- GetX: State Management untuk memudahkan development app
- Hive: menyimpan data akun yang telah login ke local
- Sizer: seperti MediaQuery, sizer lebih mudah untuk mendapatkan ukuran berdasarkan Screen

## Keperluan
- [x] Splash Screen Page
- [x] Login Page
- [x] Pop up keterangan Login

## Improvement
- [x] Log out
- [x] dapat langsung masuk dashboard jika sebelumnya telah login dan belum log out
- [x] keterangan login muncul diatas Widget TextField
