import 'package:flutter/material.dart';

class ColorPallete {
  static const greyLight = Color(0xFF999999);
  static const greyMedium = Color(0xFF8a7c70);
  static const greyDark = Color(0xFF666666);

  static const blackDark = Color(0xFF333333);
  static const blackLight = Color(0xFF4F4F4F);

  static const white = Color(0xFFFFFFFF);
  static const whiteDark = Color(0xFFCCCCCC);

  static const primaryColor = Color(0xFF7e00b3);
  static const orange = Color(0xFFfe6604);
  static const red = Color(0xFFE83C3C);

  static const blue = Color(0xFF172E7C);
}
