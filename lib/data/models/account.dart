import 'dart:convert';

import '../data_account.dart';

List<Account> accountsFromJson(String str) =>
    List<Account>.from(json.decode(str).map((x) => Account.fromJson(x)));

class Account {
  Account({
    required this.id,
    required this.password,
    required this.name,
  });

  String id;
  String password;
  String name;

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json["ID"],
        password: json["password"],
        name: json["name"],
      );
}

List<Account> getDataAccounts() {
  return accountsFromJson(DataAccount.listAccount.toString());
}
