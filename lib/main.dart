import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:infosys_app/pages/get_started_pages/login_page.dart';
import 'package:infosys_app/pages/get_started_pages/splash_screen_page.dart';
import 'package:infosys_app/pages/main_menu/dashboard_page.dart';
import 'package:sizer/sizer.dart';

void main() async {
  await Hive.initFlutter();
  runApp(InfosysApp());
}

class InfosysApp extends StatefulWidget {
  @override
  _InfosysAppState createState() => _InfosysAppState();
}

class _InfosysAppState extends State<InfosysApp> {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (_, __, ___) {
        return GetMaterialApp(
          title: 'Infosys Mobile',
          theme: ThemeData(
            appBarTheme: AppBarTheme(
              iconTheme: IconThemeData(color: Colors.black),
            ),
            primarySwatch: Colors.deepOrange,
            fontFamily: 'Poppins',
          ),
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => SplashScreen(),
            LoginPage.routeName: (context) => LoginPage(),
            DashboardPage.routeName: (context) => DashboardPage()
          },
        );
      },
    );
  }
}
