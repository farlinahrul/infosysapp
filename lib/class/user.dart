import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:infosys_app/data/models/account.dart';

class User extends GetxController with HiveObjectMixin {
  static const String boxName = 'user';
  RxString name = ''.obs;

  List<Account> accounts = getDataAccounts();

  Future<void> init() async {
    var box = await Hive.openBox(boxName);
    if (box.isNotEmpty) {
      for (var account in accounts) {
        if (box.get('userId') == account.id) {
          this.name = account.name.obs;
          break;
        }
      }
    }
  }

  Future<void> _storeAccountData(String userId, String name) async {
    var box = await Hive.openBox(boxName);
    box.put("userId", userId);
    this.name = name.obs;
  }

  Future<void> _deleteAccountData() async {
    var box = await Hive.openBox(boxName);
    box.deleteFromDisk();
    this.name = ''.obs;
  }

  Future<bool> loginUser(String userId, String password) async {
    for (var account in accounts) {
      if (account.id == userId && account.password == password) {
        await _storeAccountData(userId, account.name);
        return true;
      }
    }
    return false;
  }

  void logoutUser() {
    _deleteAccountData();
  }
}
