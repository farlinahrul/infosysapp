import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infosys_app/class/user.dart';
import 'package:infosys_app/pages/main_menu/dashboard_page.dart';
import 'package:infosys_app/services/color_pallete.dart';
import 'package:infosys_app/widgets/alert_dalog.dart';
import 'package:infosys_app/widgets/custom_text.dart';
import 'package:infosys_app/widgets/custom_button.dart';
import 'package:infosys_app/widgets/infosys_logo.dart';
import 'package:sizer/sizer.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login_page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final User user = Get.find<User>();
  final TextEditingController userId = TextEditingController();
  final TextEditingController password = TextEditingController();
  String message = '';

  Widget _titleInputField(String text) {
    return NormalText(
      text,
      letterSpacing: 0.5.sp,
      fontSize: 12.sp,
      fontWeight: FontWeight.w500,
      color: ColorPallete.blackLight,
    );
  }

  Widget _inputUserId() {
    return TextFormField(
      controller: userId,
      autofocus: true,
      keyboardType: TextInputType.name,
      style: TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
      ),
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.never,
        labelText: "User ID",
        labelStyle: TextStyle(
          fontSize: 12.sp,
          fontWeight: FontWeight.w400,
          color: ColorPallete.greyDark,
          fontStyle: FontStyle.italic,
        ),
        fillColor: Colors.white,
      ),
      textInputAction: TextInputAction.next,
    );
  }

  Widget _buttonLogin(Function showDialog) {
    return Align(
      alignment: Alignment.centerRight,
      child: CustomPrimaryButton(
        'Login',
        onPressed: () async {
          if (userId.text == '' || password.text == '') {
            message = 'User ID dan/atau Password anda belum diisi.';
            showDialog();
          } else {
            if (await user.loginUser(userId.text, password.text)) {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => DashboardPage(),
                ),
              );
            } else {
              message = 'User ID atau Password salah';
              showDialog();
            }
          }
          setState(() {});
        },
      ),
    );
  }

  Widget _signUp() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Don\'t have an account? ',
          style: TextStyle(
            fontSize: 10.sp,
            fontWeight: FontWeight.normal,
            color: ColorPallete.greyLight,
            fontFamily: 'Poppins',
          ),
          children: <TextSpan>[
            TextSpan(
              text: 'Sign Up',
              style: TextStyle(
                fontSize: 10.sp,
                fontWeight: FontWeight.w500,
                color: ColorPallete.orange,
                fontFamily: 'Poppins',
                decoration: TextDecoration.underline,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _inputPassword() {
    return TextFormField(
      controller: password,
      obscureText: true,
      autofocus: true,
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
      ),
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.never,
        labelText: "Password",
        labelStyle: TextStyle(
          fontSize: 12.sp,
          fontWeight: FontWeight.w400,
          color: ColorPallete.greyDark,
          fontStyle: FontStyle.italic,
        ),
        fillColor: Colors.white,
      ),
      textInputAction: TextInputAction.done,
    );
  }

  Widget showMessage() {
    return NormalText(
      message,
      fontSize: 8.sp,
      fontWeight: FontWeight.normal,
      color: ColorPallete.red,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Image(
              image:
                  AssetImage('assets/images/get_started_page/header-login.png'),
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.h),
            child: Column(
              children: [
                SizedBox(
                  height: 4.0.h,
                ),
                LogoImage(),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        NormalText(
                          'Login',
                          fontSize: 24.sp,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                        NormalText(
                          'Please Sign in to Continue.',
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 3.0.h,
                        ),
                        message != '' ? showMessage() : SizedBox(),
                        SizedBox(
                          height: 3.0.h,
                        ),
                        _titleInputField('User ID'),
                        _inputUserId(),
                        SizedBox(
                          height: 3.h,
                        ),
                        _titleInputField('Password'),
                        _inputPassword(),
                        SizedBox(
                          height: 5.0.h,
                        ),
                        _buttonLogin(() {
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) {
                              return OneButtonAlertDialog(
                                title: 'Login Gagal!',
                                icon: Icons.dangerous_outlined,
                                description: message,
                                buttonText: 'OK',
                                onPressed: () async {
                                  Navigator.pop(context);
                                },
                              );
                            },
                          );
                        }),
                      ],
                    ),
                  ),
                ),
                _signUp()
              ],
            ),
          )
        ],
      ),
    );
  }
}
