import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infosys_app/class/user.dart';
import 'package:infosys_app/pages/get_started_pages/login_page.dart';
import 'package:infosys_app/pages/main_menu/dashboard_page.dart';
import 'package:infosys_app/widgets/infosys_logo.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  User user = Get.put(User());
  @override
  void initState() {
    super.initState();
    user.init();
    Timer.periodic(const Duration(seconds: 2), (timer) {
      timer.cancel();
      user.name.value != ''
          ? Get.off(() => DashboardPage())
          : Get.off(() => LoginPage());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Image(
              image: AssetImage(
                  'assets/images/get_started_page/footer-splash.png'),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Image(
              image: AssetImage(
                  'assets/images/get_started_page/header-splash.png'),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: LogoImage(),
          )
        ],
      )),
    );
  }
}
