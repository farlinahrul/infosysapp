import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infosys_app/class/user.dart';
import 'package:infosys_app/services/color_pallete.dart';
import 'package:infosys_app/widgets/alert_dalog.dart';
import 'package:infosys_app/widgets/custom_text.dart';
import 'package:infosys_app/widgets/custom_button.dart';
import 'package:sizer/sizer.dart';

class DashboardPage extends StatelessWidget {
  static const routeName = '/dashboard_page';
  final User user = Get.find<User>();

  Widget _buttonLogout(Function showDialog) {
    return CustomPrimaryButton(
      'Log Out',
      onPressed: () {
        showDialog();
      },
    );
  }

  Widget _textHello() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6.0.h, horizontal: 4.0.h),
      width: 100.w,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          NormalText(
            'Hello, ${user.name.value}',
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            color: ColorPallete.blackDark,
          ),
          NormalText(
            'Selamat Datang',
            fontSize: 14.sp,
            fontWeight: FontWeight.normal,
            color: ColorPallete.blackLight,
          ),
          NormalText(
            'Akunmu akan selalu tertaut selama kamu tidak Log Out.',
            fontSize: 8.sp,
            fontWeight: FontWeight.normal,
            color: ColorPallete.blackLight,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: _textHello(),
            ),
            _buttonLogout(() {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return TwoButtonAlertDialog(
                    title: 'Log Out?',
                    icon: Icons.warning_amber_rounded,
                    description: 'Yakin ingin Log Out?',
                    leftButtonText: 'Ya',
                    rightButtonText: 'Tidak',
                    leftOnPressed: () {
                      user.logoutUser();
                      Get.offAndToNamed('/login_page');
                    },
                    rightOnPressed: () {
                      Navigator.pop(context);
                    },
                  );
                },
              );
            }),
            SizedBox(
              height: 10.h,
            )
          ],
        ),
      ),
    );
  }
}
