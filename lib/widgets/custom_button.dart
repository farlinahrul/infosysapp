import 'package:flutter/material.dart';
import 'package:infosys_app/services/color_pallete.dart';
import 'package:sizer/sizer.dart';

enum ButtonType { Small, Fit }

class CustomPrimaryButton extends StatelessWidget {
  final String label;
  final ButtonType? type;
  final Function()? onPressed;
  final bool? enable;
  final bool? rounded;
  final Color? color;

  const CustomPrimaryButton(this.label,
      {Key? key,
      this.onPressed,
      this.type = ButtonType.Small,
      this.enable = true,
      this.rounded = true,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case ButtonType.Small:
        return TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 1.5.h),
            backgroundColor: color == null ? ColorPallete.primaryColor : color,
            shape: RoundedRectangleBorder(
              borderRadius: rounded!
                  ? BorderRadius.circular(20.0)
                  : BorderRadius.circular(5.0),
            ),
            primary: Colors.white,
            elevation: 0.0,
          ),
          onPressed: onPressed,
          child: Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              letterSpacing: 0.5.sp,
              fontSize: 14.sp,
              fontWeight: FontWeight.w600,
              color: Colors.white,
              fontFamily: 'Poppins',
            ),
          ),
        );
      case ButtonType.Fit:
        return TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.h),
            backgroundColor: color == null ? ColorPallete.primaryColor : color,
            shape: RoundedRectangleBorder(
              borderRadius: rounded!
                  ? BorderRadius.circular(20.0)
                  : BorderRadius.circular(5.0),
            ),
            primary: Colors.white,
            elevation: 0.0,
          ),
          onPressed: onPressed,
          child: Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              letterSpacing: 0.5.sp,
              fontSize: 14.sp,
              fontWeight: FontWeight.w600,
              color: Colors.white,
              fontFamily: 'Poppins',
            ),
          ),
        );
      default:
        return SizedBox();
    }
  }
}
