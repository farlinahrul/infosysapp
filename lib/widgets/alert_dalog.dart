import 'package:flutter/material.dart';
import 'package:infosys_app/services/color_pallete.dart';
import 'package:sizer/sizer.dart';

import 'custom_text.dart';
import 'custom_button.dart';

class OneButtonAlertDialog extends StatelessWidget {
  final String title;
  final String description;
  final IconData icon;
  final String buttonText;
  final void Function() onPressed;

  const OneButtonAlertDialog({
    Key? key,
    required this.title,
    required this.icon,
    required this.description,
    required this.buttonText,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 2.0.h),
              child: Icon(
                icon,
                size: 80,
                color: ColorPallete.red,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 1.0.h),
              child: NormalText(
                title,
                textAlign: TextAlign.center,
                fontSize: 14.0.sp,
                fontWeight: FontWeight.bold,
                color: ColorPallete.blue,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 3.0.h),
              child: NormalText(
                description,
                fontSize: 12.0.sp,
                fontWeight: FontWeight.w400,
                color: Colors.black,
                textAlign: TextAlign.center,
              ),
            ),
            CustomPrimaryButton(
              buttonText,
              color: ColorPallete.orange,
              onPressed: onPressed,
            ),
          ],
        ),
      ),
    );
  }
}

class TwoButtonAlertDialog extends StatelessWidget {
  final String title;
  final IconData icon;
  final String description;
  final String leftButtonText;
  final String rightButtonText;
  final void Function() leftOnPressed;
  final void Function() rightOnPressed;

  const TwoButtonAlertDialog({
    Key? key,
    required this.title,
    required this.icon,
    required this.description,
    required this.leftButtonText,
    required this.rightButtonText,
    required this.leftOnPressed,
    required this.rightOnPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 2.0.h),
              child: Icon(
                icon,
                size: 80,
                color: ColorPallete.red,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 1.0.h),
              child: NormalText(
                title,
                textAlign: TextAlign.center,
                fontSize: 14.0.sp,
                fontWeight: FontWeight.bold,
                color: ColorPallete.blue,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 3.0.h),
              child: NormalText(
                description,
                fontSize: 12.0.sp,
                fontWeight: FontWeight.w400,
                color: Colors.black,
                textAlign: TextAlign.center,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: CustomPrimaryButton(
                    leftButtonText,
                    rounded: false,
                    type: ButtonType.Fit,
                    color: ColorPallete.orange,
                    onPressed: leftOnPressed,
                  ),
                ),
                SizedBox(width: 2.0.w),
                Expanded(
                  child: CustomPrimaryButton(
                    rightButtonText,
                    rounded: false,
                    type: ButtonType.Fit,
                    color: ColorPallete.orange,
                    onPressed: rightOnPressed,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
